package de.rohedaten.interstellartravel;

import de.rohedaten.interstellartravel.io.FileFormatException;
import de.rohedaten.interstellartravel.io.JSONImporter;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;
import picocli.CommandLine;

import java.io.FileNotFoundException;

public class InterstellarTravel {

    @CommandLine.Option(names = {"-f", "--file"}, description = "The JSON file representing the graph", required = true)
    private String inputFile;
    @CommandLine.Option(names = {"-s", "--source"}, description = "The name of the source planet/vertex. Default: \"Erde\"", defaultValue = "Erde")
    private String sourcePlanet;
    @CommandLine.Option(names = {"-d", "--dest"}, description = "The name of the destination planet/vertex. Default: \"b3-r7-r4nd7\"", defaultValue = "b3-r7-r4nd7")
    private String destinationPlanet;
    @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true, description = "display this help message")
    private boolean usageHelpRequested;
    /**
     * The graph representing the universe
     */
    private SimpleWeightedGraph<Vertex, DefaultWeightedEdge> universe;

    public static void main(String[] args) {

        InterstellarTravel interstellarTravel = new InterstellarTravel();
        try {
            CommandLine.populateCommand(interstellarTravel, args);
        } catch (CommandLine.MissingParameterException e) {
            CommandLine.usage(interstellarTravel, System.out);
            return;
        }
        if (interstellarTravel.usageHelpRequested) {
            CommandLine.usage(interstellarTravel, System.out);
            return;
        }
        interstellarTravel.generateGraph();
        interstellarTravel.travel();
    }

    /**
     * Parse the given {@link #inputFile} and populate the Graph {@link #universe} with the given vertices end edges
     */
    private void generateGraph() {

        JSONImporter importer = null;
        try {
            importer = new JSONImporter(inputFile);
        } catch (FileNotFoundException e) {
            System.err.println("File " + inputFile + " not found");
            System.exit(1);
        }
        try {
            universe = importer.loadUniverse();
        } catch (FileFormatException e) {
            System.err.println("File " + inputFile + " malformed");
        }
    }

    /**
     * Get Shortest path form the given {link {@link #sourcePlanet}} to the {@link #destinationPlanet} using the Dijkstra shortest path algorithm
     */
    private void travel() {

        Vertex source = null;
        try {
            source = universe.vertexSet().stream().filter(vertex -> sourcePlanet.equals(vertex.getName())).findAny().orElseThrow(FileFormatException::new);
        } catch (FileFormatException e) {
            System.err.println("Source planet " + sourcePlanet + " not found in File");
            System.exit(1);
        }
        Vertex dest = null;
        try {
            dest = universe.vertexSet().stream().filter(vertex -> destinationPlanet.equals(vertex.getName())).findAny().orElseThrow(FileFormatException::new);
        } catch (FileFormatException e) {
            System.err.println("Destination planet " + destinationPlanet + " not found in File");
            System.exit(1);
        }
        DijkstraShortestPath<Vertex, DefaultWeightedEdge> dijkstraAlg =
                new DijkstraShortestPath<>(universe);
        GraphPath<Vertex, DefaultWeightedEdge> path = dijkstraAlg.getPath(source, dest);

        System.out.println("Shortest path from " + sourcePlanet + " to " + destinationPlanet + ":");
        System.out.println("Path: " + path.getVertexList());
        System.out.println("Total cost: " + path.getWeight() + ", length: " + path.getLength() + "\n");
    }
}
