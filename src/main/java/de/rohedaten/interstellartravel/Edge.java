package de.rohedaten.interstellartravel;

/**
 * Simple edge Data class. For import purposes only
 */
public class Edge {
    private int source;
    private int destinaiton;
    private double cost;

    public Edge(int source, int destination, double cost) {
        this.source = source;
        this.destinaiton = destination;
        this.cost = cost;
    }

    public int getSource() {
        return source;
    }

    public int getDestinaiton() {
        return destinaiton;
    }

    public double getCost() {
        return cost;
    }
}
