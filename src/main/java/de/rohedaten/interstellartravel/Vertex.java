package de.rohedaten.interstellartravel;

/**
 * Vertex Data class
 */
public class Vertex {

    private int number;
    private String name;

    public Vertex(int number, String label) {
        this.number = number;
        this.name = label;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
