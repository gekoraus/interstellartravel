package de.rohedaten.interstellartravel.io;

import de.rohedaten.interstellartravel.Edge;
import de.rohedaten.interstellartravel.Vertex;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for importing the vertices and edges into a {@link SimpleWeightedGraph}
 */
public class JSONImporter {

    private JSONObject jsonObject;

    public JSONImporter(String filepath) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(
                new FileReader(filepath));
        JSONTokener tokener = new JSONTokener(br);
        jsonObject = new JSONObject(tokener);
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the graph representing the vertices and edges saved in the JSON file
     * @throws FileFormatException thrown if the file is malformed
     */
    public SimpleWeightedGraph<Vertex, DefaultWeightedEdge> loadUniverse() throws FileFormatException {
        SimpleWeightedGraph<Vertex, DefaultWeightedEdge> universe = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);

        for (Vertex planet : getPlanets()
        ) {
            universe.addVertex(planet);
        }
        for (Edge edge : getEdges()
        ) {
            Vertex edgeSource = universe.vertexSet().stream().filter(vertex -> edge.getSource() == vertex.getNumber()).findAny().orElseThrow(FileFormatException::new);
            Vertex edgeDest = universe.vertexSet().stream().filter(vertex -> edge.getDestinaiton() == vertex.getNumber()).findAny().orElseThrow(FileFormatException::new);
            DefaultWeightedEdge weightedEdge = new DefaultWeightedEdge();
            universe.addEdge(edgeSource, edgeDest, weightedEdge);
            universe.setEdgeWeight(weightedEdge, edge.getCost());
        }
        return universe;
    }

    private List<Vertex> getPlanets() {

        JSONArray array = jsonObject.getJSONArray("nodes");

        List<Vertex> planets = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            String label = array.getJSONObject(i).getString("label");
            planets.add(new Vertex(i, label));
        }
        return planets;
    }

    private List<Edge> getEdges() {

        JSONArray array = jsonObject.getJSONArray("edges");

        List<Edge> edges = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            Edge edge = new Edge(array.getJSONObject(i).getInt("source"),
                    array.getJSONObject(i).getInt("target"),
                    array.getJSONObject(i).getDouble("cost"));
            edges.add(edge);
        }
        return edges;
    }
}
