package de.rohedaten.interstellartravel.io;

/**
 * Exception to indicate that the format of the io file is invalid
 */
public class FileFormatException extends Exception {

    public FileFormatException() {
    }

    public FileFormatException(String s) {
        super(s);
    }
}
