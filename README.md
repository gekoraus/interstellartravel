# Interstellar travel
Contribution to the [get in IT coding challenge](https://www.get-in-it.de/coding-challenge).

The built jar can be obtained here: [interstellartravel-1.0.jar](/uploads/9ea2c5743c3e017ff1ac389af8f309bb/interstellartravel-1.0.jar)

## Objective

Find the shortest path between the nodes "Erde" and "b3-r7-r4nd7" in an undirected weighted graph. The graph is provided via a json file by the organizers. 

## Solution

The solution as it is outputted by the program:

Path: 

[Erde, node_810, node_595, node_132, node_519, node_71, node_432, b3-r7-r4nd7]

Total cost: 2.995687895999458, length: 7


## Usage

To solve the coding challenge, call the program giving it the path to the input json file. i.e.:

`java -jar interstellartravel-1.0.jar --file generatedGraph.json`

You can also get the shortest path between different vertices. Just call the program with the corresponding flags. i.e. 

`java -jar interstellartravel-1.0.jar --file generatedGraph.json --source "node_1" --dest "node_2" `

### Synopsis

```
interstellartravel-1.0.jar [-h] [-d=<destinationPlanet>] -f=<inputFile>
                      [-s=<sourcePlanet>]
    -d, --dest=<destinationPlanet>
                             The name of the destination planet/vertex. Default:
                               "b3-r7-r4nd7"
    -f, --file=<inputFile>   The JSON file representing the graph
    -h, --help               display this help message
    -s, --source=<sourcePlanet>
                             The name of the source planet/vertex. Default: "Erde"
```

### Design principles

The whole code follows this main rule:

**Use well-tested, well-optimized code instead of reinventing the wheel.**

For parsing the json file the library [JSON-java (org.json)](https://github.com/stleary/JSON-java) is used. 

The data is then parsed into a _SimpleWeightedGraph_ provided by the graph library [JGraphT](https://jgrapht.org/). JGraphT is also held responsible for calculating the shortest path using Dijkstra's shortest path algorithm.

To make the program a bit more versatile, the CLI library [picocli](https://picocli.info/) was added. this library handles user input. The user can choose, between which nodes he wants to calculate the shortest path (see [Usage](#Usage)).